#include "AIPlayer.h"

#include "Field.h"

#include <cassert>
#include <limits>

namespace {
    auto MIN_VALUE = std::numeric_limits<int>::min();
    auto MAX_VALUE = std::numeric_limits<int>::max();
} // namespace

AIPlayer::AIPlayer(bool upper, Field const & f, char const * name, int depth, WeightFunction function)
    : ArtificialIntelligence(upper, f, name)
    , depth(depth)
    , function(function)
{}

int AIPlayer::getTurn()
{
    return algorithm(field.clone(), isUpper, depth, function);
}

int AIPlayer::algorithm(pField const field, bool upper, int depth, WeightFunction function)
{
    auto bestMove = -1;
    assert(depth > 0 && depth % 2 == 0);
    auto evalution = AlphaBetaPruning(field, upper, MIN_VALUE, MAX_VALUE, depth, function, bestMove);
    assert(bestMove >= 0);
    return bestMove;
}

double AIPlayer::AlphaBetaPruning(pField const field, bool upper, double alpha, double beta, int depth,
                                  WeightFunction function, int & bestMove) const
{
    assert(field->isUpperTurnNow() == upper);

    if (depth == 0)
        return weightFunction(*field, upper, function);

    double scoreBest = alpha;
    double score = MIN_VALUE;
    auto chosenMove = -1;

    auto begin = 0;
    auto end = 0;
    getBounds(*field, upper, begin, end);
    for (auto i = begin; i <= end; ++i) {
        if (field->getCell(i) != 0) {
            chosenMove = i;
            break;
        }
    }

    for (; begin <= end; ++begin) {
        if (field->getCell(begin) == 0) {
            continue;
        }
        auto new_field = field->clone();
        auto result = new_field->make_turn(begin);
        assert(result != trImpossible);

        if (result == trOver) {
            score = weightFunction(*field, upper, function);
        } else if (result == trOK) {
            score = -AlphaBetaPruning(new_field, !upper, -beta, -scoreBest, depth - 1, function, bestMove);
        } else {
            score = AlphaBetaPruning(new_field, upper, scoreBest, beta, depth - 1, function, bestMove);
        }

        if (score > scoreBest) {
            scoreBest = score;
            chosenMove = begin;
        }

        if (scoreBest >= beta)
            break;
    }

    bestMove = chosenMove;
    return scoreBest;
}
