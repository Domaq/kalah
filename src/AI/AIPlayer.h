#pragma once

#include "ArtificialIntelligence.h"

class AIPlayer final : public ArtificialIntelligence
{
public:
    explicit AIPlayer(bool upper, Field const & f, const char * name, int depth, WeightFunction function);

    int getTurn() override;

protected:
    int algorithm(pField const field, bool upper, int depth, WeightFunction function) override;

private:
    int depth;
    WeightFunction function;

    double AlphaBetaPruning(pField const field, bool upper, double alpha, double beta, int depth,
                            WeightFunction function, int & bestMove) const;
};
