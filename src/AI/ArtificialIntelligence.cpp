#include "ArtificialIntelligence.h"

#include <cassert>
#include <numeric>

ArtificialIntelligence::ArtificialIntelligence(bool upper, const Field & f, const char * name)
    : Player(upper, f, name)
{}

void ArtificialIntelligence::getBounds(const Field & field, bool isUpper, int & begin, int & end)
{
    begin = isUpper ? 0 : field.cellsForPlayer + 1;
    end = isUpper ? field.cellsForPlayer - 1 : 2 * field.cellsForPlayer;
}

double ArtificialIntelligence::weightFunction(const Field & field, bool isUpper, WeightFunction function) const
{
    double evalution = 0;

    switch (function) {
    case easyFunction:
        evalution = easyWeightFunction(field, isUpper);
        break;
    case AbayAndArtyom:
        evalution = AbayAndArtyomWeightFunction(field, isUpper);
        break;
    case customFunction1:
        evalution = customWeightFunctionFirst(field, isUpper);
        break;
    case customFunction2:
        evalution = customWeightFunctionSecond(field, isUpper);
        break;
    default:
        assert(0);
    }

    return evalution;
}

void ArtificialIntelligence::accumulateBaseCoefficients(const Field & field, int & Kh, int & Kc, int & Nh, int & Nc,
                                                        bool isUpper) const
{
    Kh = field.cells[field.cellsForPlayer];
    Kc = field.cells[2 * field.cellsForPlayer + 1];

    Nh = std::accumulate(field.cells.begin(), field.cells.begin() + field.cellsForPlayer, 0);

    Nc = std::accumulate(field.cells.begin() + field.cellsForPlayer + 1,
                         field.cells.begin() + 2 * field.cellsForPlayer + 1, 0);

    if (!isUpper) {
        std::swap(Nc, Nh);
        std::swap(Kh, Kc);
    }
}

int ArtificialIntelligence::accumulateActivityCoefficient(const Field & field, bool isUpper)
{
    auto begin = isUpper ? 0 : field.cellsForPlayer + 1;
    auto end = isUpper ? field.cellsForPlayer - 1 : 2 * field.cellsForPlayer;

    auto D = 0;
    for (auto count_cells = field.cellsForPlayer; begin <= end; ++begin, --count_cells) {
        const auto tmp = field.cellsForPlayer - begin % (field.cellsForPlayer + 1) - 1;
        auto item = field.cells[begin];
        item = (item - tmp - 1) % (2 * field.cellsForPlayer + 1);

        D += tmp;
        if (item <= field.cellsForPlayer) {
            D -= item;
        } else {
            D -= field.cellsForPlayer;
            D += item - field.cellsForPlayer;
        }
        D += field.cellsForPlayer + 1;
    }

    return D;
}

double ArtificialIntelligence::easyWeightFunction(const Field & field, bool isUpper) const
{
    int Kh = 0;
    int Kc = 0;
    int Nh = 0;
    int Nc = 0;

    accumulateBaseCoefficients(field, Kh, Kc, Nh, Nc, isUpper);

    auto evalution = (Kh - Kc) * 100.0 + Nh - Nc;

    return evalution;
}

double ArtificialIntelligence::AbayAndArtyomWeightFunction(const Field & field, bool isUpper) const
{
    int Kh = 0;
    int Kc = 0;
    int Nh = 0;
    int Nc = 0;

    accumulateBaseCoefficients(field, Kh, Kc, Nh, Nc, isUpper);

    int Dh = accumulateActivityCoefficient(field, isUpper);
    int Dc = accumulateActivityCoefficient(field, !isUpper);

    auto evalution = (Kh + 17.3 / (37 - Kh) - 40 / Dh) - (Kc + 17.3 / (37 - Kc) - 40 / Dc);

    return evalution;
}

double ArtificialIntelligence::customWeightFunctionFirst(const Field & field, bool isUpper) const
{
    int Kh = 0;
    int Kc = 0;
    int Nh = 0;
    int Nc = 0;

    accumulateBaseCoefficients(field, Kh, Kc, Nh, Nc, isUpper);

    int Dh = accumulateActivityCoefficient(field, isUpper);
    int Dc = accumulateActivityCoefficient(field, !isUpper);

    ////
    auto evalution = (Kh - Kc) + Nh - Nc;

    return evalution;
}

double ArtificialIntelligence::customWeightFunctionSecond(const Field & field, bool isUpper) const
{
    int Kh = 0;
    int Kc = 0;
    int Nh = 0;
    int Nc = 0;

    accumulateBaseCoefficients(field, Kh, Kc, Nh, Nc, isUpper);

    int Dh = accumulateActivityCoefficient(field, isUpper);
    int Dc = accumulateActivityCoefficient(field, !isUpper);

    ////
    auto evalution = (Kh - Kc) * 100.0 + Nh - Nc;

    return evalution;
}
