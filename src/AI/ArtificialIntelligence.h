#pragma once

#include "Field.h"
#include "Player.h"

enum WeightFunction
{
    easyFunction,
    AbayAndArtyom,
    customFunction1,
    customFunction2
};

class ArtificialIntelligence : public Player
{
public:
    ArtificialIntelligence(bool upper, Field const & f, char const * name);

    static void getBounds(Field const & field, bool isUpper, int & begin, int & end);

    virtual double weightFunction(Field const & field, bool isUpper, WeightFunction function) const;

    int getTurn() override = 0;

protected:
    virtual int algorithm(pField const field, bool upper, int depth, WeightFunction function) = 0;

private:
    void accumulateBaseCoefficients(Field const & field, int & Kh, int & Kc, int & Nh, int & Nc, bool isUpper) const;

    static int accumulateActivityCoefficient(const Field & field, bool isUpper);

    double easyWeightFunction(Field const & field, bool isUpper) const;

    double AbayAndArtyomWeightFunction(Field const & field, bool isUpper) const;

    double customWeightFunctionFirst(Field const & field, bool isUpper) const;

    double customWeightFunctionSecond(Field const & field, bool isUpper) const;
};
