#include "ConsoleGame.h"

#include <iostream>

ConsoleGame::ConsoleGame(Field & f, Player & u, Player & l)
    : field(f)
    , lower(l)
    , upper(u)
{}

void ConsoleGame::play() const
{
    TurnResult result;
    do {
        std::cout << field;

        auto & currPlayer = getCurrentPlayer();

        auto turn = currPlayer.getTurn();
        if (turn == -1) {
            turn = enterTurn();
        }

        std::cout << getCurrentPlayer().getName() << " turn has done: " << turn << std::endl;
        result = field.make_turn(turn);

        if (result == trImpossible) {
            std::cout << "this turn is impossible" << std::endl;
        }

        if (result == trRepeat) {
            std::cout << "turn again" << std::endl;
        }
    } while (result != trOver);

    field.takeRemaningStones();
    std::cout << field;
    std::cout << "game over" << std::endl;
    auto diff = field.getDifference();

    if (diff > 0) {
        std::cout << "winner: " << lower.getName() << "  with difference: " << diff << std::endl;
    } else if (diff < 0) {
        std::cout << "winner: " << upper.getName() << "  with difference: " << -diff << std::endl;
    } else {
        std::cout << "draw" << std::endl;
    }
}

Player & ConsoleGame::getCurrentPlayer() const
{
    return field.isUpperTurnNow() ? upper : lower;
}

int ConsoleGame::enterTurn() const
{
    std::cout << getCurrentPlayer().getName() << ", your turn: ";
    auto turn = 0;
    std::cin >> turn;
    std::cout << std::endl;
    return turn;
}
