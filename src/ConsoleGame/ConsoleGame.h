#pragma once

#include "Field.h"
#include "Player.h"

class ConsoleGame final
{
public:
    explicit ConsoleGame(Field & f, Player & u, Player & l);

    void play() const;

private:
    Field & field;
    Player & lower;
    Player & upper;

    Player & getCurrentPlayer() const;

    int enterTurn() const;
};
