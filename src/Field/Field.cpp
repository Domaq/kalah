#include "Field.h"

Field::Field(int sCellsForPlayer, int stones)
    : cellsForPlayer(sCellsForPlayer)
{
    cells.resize(2 * cellsForPlayer + 2, stones);
    cells[cellsForPlayer] = 0;
    cells[2 * cellsForPlayer + 1] = 0;
}

Field::Field(const Field & other)
    : cellsForPlayer(other.cellsForPlayer)
    , isUpperTurn(other.isUpperTurn)
    , isOver(other.isOver)
{
    cells.resize(2 * cellsForPlayer + 2, 0);
    std::copy(other.cells.begin(), other.cells.end(), cells.begin());
}

TurnResult Field::make_turn(int index)
{
    if (isOver) {
        return trOver;
    }

    if (!isSelfCell(isUpperTurn, index)) {
        return trImpossible;
    }

    if (cells[index] == 0) {
        return trImpossible;
    }
	
    index = distribStones(index, getKalahIndex(!isUpperTurn));

    if (cells[index] == 1 && isSelfCell(isUpperTurn, index)) {
        takeOpposite(index, getKalahIndex(isUpperTurn));
    }

    auto repeat = index == getKalahIndex(isUpperTurn);
    if (!repeat) {
        isUpperTurn = !isUpperTurn;
    }

    if (checkTurn()) {
        return trOver;
    }

    return repeat ? trRepeat : trOK;
}

bool Field::isUpperTurnNow() const
{
    return isUpperTurn;
};

int Field::getDifference() const
{
    return cells[getKalahIndex(false)] - cells[getKalahIndex(true)];
}

void Field::takeRemaningStones()
{
    auto remaningStones = 0;
    for (int i = 0, n = cells.size(); i < n; ++i) {
        if (isSelfCell(!isUpperTurn, i)) {
            remaningStones += cells[i];
            cells[i] = 0;
        }
    }

    cells[getKalahIndex(!isUpperTurn)] += remaningStones;
}

std::ostream & operator<<(std::ostream & os, const Field & field)
{
    for (int i = 0, n = field.cells.size(); i < field.cellsForPlayer; ++i) {
        os << "\t" << field.cells[n - i - 2];
    }

    os << "\n" << field.cells[field.getKalahIndex(false)];
    os.fill('\t');

    os.width(field.cellsForPlayer + 1);
    os << "\t" << field.cells[field.getKalahIndex(true)] << "\n";

    for (auto i = 0; i < field.cellsForPlayer; ++i) {
        os << "\t" << field.cells[i];
    }
    os << "\n";

    return os;
}

pField Field::clone() const
{
    return std::shared_ptr<Field>(new Field(*this));
}

int Field::distribStones(int index, int kalah)
{
    auto stones = cells[index];
    cells[index] = 0;
    int cellsNumber = cells.size();
    for (; stones > 0; --stones) {
        ++index;

        if (index == kalah) {
            ++index;
        }

        if (index == cellsNumber) {
            index = 0;
        }

        ++cells[index];
    }

    return index;
}

bool Field::checkTurn()
{
    auto ourStones = 0;
    for (int i = 0, n = cells.size(); i < n; ++i) {
        if (isSelfCell(isUpperTurn, i)) {
            ourStones += cells[i];
        }
    }

    return ourStones == 0;
}

bool Field::isSelfCell(bool isUpper, int index) const
{
    if (isUpper && index >= 0 && index <= cellsForPlayer - 1) {
        return true;
    } else if (!isUpper && index >= cellsForPlayer + 1 && index <= 2 * cellsForPlayer) {
        return true;
    } else {
        return false;
    }
};

int Field::getKalahIndex(bool isUpper) const
{
    return isUpper ? cellsForPlayer : 2 * cellsForPlayer + 1;
}

// 0-6 1-5 2 4
void Field::takeOpposite(int index, int kalah)
{
    int n = cells.size();
    cells[kalah] += cells[index];
    cells[kalah] += cells[n - index - 2];
    cells[index] = 0;
    cells[n - index - 2] = 0;
}
