#pragma once

#include <iostream>
#include <memory>
#include <vector>

class Field;

typedef std::shared_ptr<Field> pField;

enum TurnResult
{
    // ��� ����������
    trImpossible,
    // ��� ������, ��������� ��� ����������
    trOK,
    // ��� ������, ��� ����� ���������
    trRepeat,
    // ��� ������, ���� ��������
    trOver
};

class Field
{
public:
    explicit Field(int cellsForPlayer, int stones);

    TurnResult make_turn(int index);

    bool isUpperTurnNow() const;
    int getDifference() const;
    void takeRemaningStones();

    friend std::ostream & operator<<(std::ostream &, const Field &);

    pField clone() const;

    int getCell(size_t index)
    {
        return cells[index];
    }

private:
    explicit Field(const Field & other);

    int distribStones(int index, int kalah);
    bool isSelfCell(bool isUpper, int index) const;
    int getKalahIndex(bool isUpper) const;
    void takeOpposite(int index, int kalah);
    bool checkTurn();

    friend class ArtificialIntelligence;

private:
    std::vector<int> cells;
    int cellsForPlayer;

    bool isUpperTurn{true};
    bool isOver{false};
};
