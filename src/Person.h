#pragma once

#include "Player.h"

class Person : public Player
{
public:
    explicit Person(bool upper, const Field & f, const char * name);

    int getTurn() override;
};
