#pragma once

#include <string>

class Field;

class Player
{
public:
    explicit Player(bool upper, const Field & f, const char * name)
        : isUpper(upper)
        , field(f)
        , name(name)
    {}

    virtual ~Player() = default;

    virtual const char * getName()
    {
        return name.c_str();
    }

    virtual int getTurn() = 0;

protected:
    const bool isUpper;
    const Field & field;
    const std::string name;
};
