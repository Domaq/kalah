#include "ConsoleGame.h"

#include "AIPlayer.h"
#include "Person.h"

#include <iostream>
#include <string>

void printUsage(int argc, char ** argv);
bool checkCMD(int N, int playerFirst, int playerSecond, int evaluationFunctionFirst, int evaluationFunctionSecond,
              int depthFirst, int depthSecond);

int runGame(int N, int playerFirst, int playerSecond, const char * nameFirst, const char * nameSecond, int depthFirst,
            int depthSecond, int evaluationFunctionFirst, int evaluationFunctionSecond)
{
    Field field(N, N);
    Player * first;
    Player * second;

    if (playerFirst) {
        first = new AIPlayer(true, field, nameFirst, depthFirst, static_cast<WeightFunction>(evaluationFunctionFirst));
    } else {
        first = new Person(true, field, nameFirst);
    }

    if (playerSecond) {
        second =
            new AIPlayer(false, field, nameSecond, depthSecond, static_cast<WeightFunction>(evaluationFunctionSecond));
    } else {
        second = new Person(false, field, nameSecond);
    }

    ConsoleGame consoleGame(field, *first, *second);
    consoleGame.play();

    delete first;
    delete second;
    return -field.getDifference();
}

int main(int argc, char ** argv)
{
    if (argc != 10) {
        printUsage(argc, argv);
        return 0;
    }

    const auto N = atoi(argv[1]);

    const auto playerFirst = atoi(argv[2]);
    const auto playerSecond = atoi(argv[3]);

    const auto nameFirst = argv[4];
    const auto nameSecond = argv[5];

    const auto evaluationFunctionFirst = atoi(argv[6]);
    const auto evaluationFunctionSecond = atoi(argv[7]);

    const auto depthFirst = atoi(argv[8]);
    const auto depthSecond = atoi(argv[9]);

    if (!checkCMD(N, playerFirst, playerSecond, evaluationFunctionFirst, evaluationFunctionSecond, depthFirst,
                  depthSecond)) {
        printUsage(argc, argv);
        return 1;
    }

    const auto diff1 = runGame(N, playerFirst, playerSecond, nameFirst, nameSecond, depthFirst, depthSecond,
                         evaluationFunctionFirst, evaluationFunctionSecond);
    const auto diff2 = runGame(N, playerSecond, playerFirst, nameSecond, nameFirst, depthSecond, depthFirst,
                         evaluationFunctionSecond, evaluationFunctionFirst);
    const auto diff = diff1 - diff2;

    std::cout << "First game: " << nameFirst << " vs " << nameSecond << ": " << diff1 << "\n"
              << "Second game: " << nameSecond << " vs " << nameFirst << ": " << diff2 << "\n";

    if (diff > 0) {
        std::cout << "winner: " << nameFirst << "  with difference: " << diff << std::endl;
    } else if (diff < 0) {
        std::cout << "winner: " << nameSecond << "  with difference: " << -diff << std::endl;
    } else {
        std::cout << "draw" << std::endl;
    }

    return 0;
}

void printUsage(int argc, char ** argv)
{
    std::cout
        << argv[0] << "[options]" << std::endl
        << "Options:" << std::endl
        << "N                          Count cells (2, 3, 4, 5, 6)" << std::endl
        << "Player 1                   Person or AI (0, 1)" << std::endl
        << "Player 2                   Person or AI (0, 1)" << std::endl
        << "Name player 1              (string)" << std::endl
        << "Name player 2              (string)" << std::endl
        << "Evaluation function 1      (0 - easyFunction, 1 - AbayAndArtyom, 2 - customFunction1, 3 - customFunction2)"
        << std::endl
        << "Evaluation function 2      (0 - easyFunction, 1 - AbayAndArtyom, 2 - customFunction1, 3 - customFunction2)"
        << std::endl
        << "Depth 1                    (2, 4, 6, 8)" << std::endl
        << "Depth 2                    (2, 4, 6, 8)" << std::endl
        << "";
}

bool checkCMD(int N, int playerFirst, int playerSecond, int evaluationFunctionFirst, int evaluationFunctionSecond,
              int depthFirst, int depthSecond)
{
    if (!(N >= 2 && N <= 6)) {
        return false;
    }

    if (!(depthFirst % 2 == 0 && depthFirst >= 2 && depthFirst <= 8)) {
        return false;
    }

    if (!(depthSecond % 2 == 0 && depthSecond >= 2 && depthSecond <= 8)) {
        return false;
    }

    if (!(evaluationFunctionFirst == 0 || evaluationFunctionFirst == 1 || evaluationFunctionFirst == 2 ||
          evaluationFunctionFirst == 3)) {
        return false;
    }

    if (!(evaluationFunctionSecond == 0 || evaluationFunctionSecond == 1 || evaluationFunctionSecond == 2 ||
          evaluationFunctionSecond == 3)) {
        return false;
    }

    if (!(playerFirst == 0 || playerFirst == 1)) {
        return false;
    }

    if (!(playerSecond == 0 || playerSecond == 1)) {
        return false;
    }

    return true;
}
